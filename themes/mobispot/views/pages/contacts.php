<?php $this->pageTitle = Yii::t('phone', 'Наши контакты'); ?>
<?php $this->mainBackground = 'main_bg_1.jpg'?>

<div class="content-wrapper">
    <div class="content-block contacts">
        <div class="row" style="font-size: 18px">
            <div class="large-12 columns form-block">
                <div class="row header">
                    <div class="column large-12">
                        <h2 class="color">
                            Наши контакты
                        </h2>
                    </div>
                </div>

                <p>
                    <b>E-mail:</b> <a href="mailto:hola@mobispot.com">hola@mobispot.com</a>
                </p>
                <p>
                    <b>График работы:</b> Понедельник - пятница с 10:00 до 18:00
                </p>
            </div>
        </div>
    </div>
</div>
<div class="fc"></div>
